import org.junit.Test;

import static org.junit.Assert.*;

public class WeightedAverageTest {

    @Test(timeout = 10000)
    public void testcase01() {
        // Arrange
        WeightedAverage homework = new WeightedAverage();
        int[] grades = new int[]{80, 85, 78};
        int[] credit = new int[]{3, 2, 3};

        // Act
        double ans = 80.5;
        double output = homework.solve(grades, credit);

        // Assert
        assertEquals(ans, output, 1e-6);
    }

}